
Esse projeto envolve investigar ferramentas livres para a criação de Cadernos de Pesquisa Abertos ([Open Science Notebook](https://wikipedia.org/wiki/Open_notebook_science)),
com foco em características adequadas as necessidade da área de matemática pura e afins
([Mais informações](https://pt.wikiversity.org/wiki/Pesquisa:_Ferramentas_livres_para_cadernos_de_pesquisas_abertos_-_matem%C3%A1tica_pura_e_%C3%A1reas_afins)).

